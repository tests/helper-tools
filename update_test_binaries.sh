#!/bin/bash

set -ex

branch=master
workdir=${TMPDIR:-/tmp}

usage()
{
	echo "usage: $0 -l RELEASE_VERSION -r REPOSITORY [-b BRANCH] | [-h]"
	echo
	echo "    -l, --release RELEASE_VERSION  The release version to download packages/binaries from (see https://repositories.apertis.org/apertis/dists/)"
	echo "    -r, --repository REPOSITORY    The name of the repository to update (under https://gitlab.apertis.org/tests)"
	echo "    -b, --branch BRANCH            The branch in the repository to push the changes to (default: master)"
	echo "    -w, --workdir WORKDIR          The workdir (default: $workdir)"
	echo "    -n, --dry-run                  Do not attempt to push changes to repository (default: push)"
	echo "    -h, --help                     Display this help and exit"
}

while [ "$1" != "" ]; do
	case $1 in
		-l | --release )	shift
							release=$1
							;;
		-r | --repository )	shift
							repository=$1
							;;
		-b | --branch )		shift
							branch=$1
							;;
		-w | --workdir )	shift
							workdir=$1
							;;
		-n | --dry-run )	dry_run=1
							;;
		-h | --help )		usage
							exit 1
							;;
	esac
	shift
done

if [ -z "$release" -o -z "$repository" ] ; then
	usage
	exit 1
fi

repository=$(readlink -f "${repository}")

if [ -n "${workdir}" ]; then
	mkdir -p "${workdir}"
	pushd "${workdir}"
fi

RELEASE=${release}
BRANCH=${branch}
TEST=$(basename ${repository})
SOURCES="target development"
ARCHS="amd64 armhf arm64"
PACKAGES=""
BINARIES=""

trim()
{
    local trimmed="$1"

    # Strip leading space.
    trimmed="${trimmed## }"
    # Strip trailing space.
    trimmed="${trimmed%% }"

    echo "$trimmed"
}

download_packages_description() {
	for SOURCE in ${SOURCES} ; do
		[ -f Packages.${RELEASE}.${ARCH}.${SOURCE} ] && continue
		wget https://repositories.apertis.org/apertis/dists/${RELEASE}/${SOURCE}/binary-${ARCH}/Packages -O Packages.${RELEASE}.${ARCH}.${SOURCE}
	done
}

get_package() {
	local PACKAGE=$1
	for SOURCE in ${SOURCES} ; do
		FILENAME=$(sed -n -e "/^Package: ${PACKAGE}$/,/^Description:/  s/Filename: \(.*\)/\1/p" Packages.${RELEASE}.${ARCH}.${SOURCE}| head -n1)
		BASEFILENAME=$(basename "${FILENAME}")
		if [ -n "${FILENAME}" -a ! -f "${BASEFILENAME}" ]; then
			wget https://repositories.apertis.org/apertis/${FILENAME}
		fi
	done
}

download_extract_packages () {
	local PACKAGE=""
	mkdir -p ${EXTRACT}

	while read -r line ; do
		# skip lines starting with "#"
		[[ "$line" =~ ^#.*$ ]] && continue

		IFS=" " read -r PACKAGE GLOB AVAILABLE_OPTIONS <<< $line
		IFS="," read -r -a AVAILABLE_OPTIONS <<< $AVAILABLE_OPTIONS

		local REQUESTED_ARCHS=
		for i in ${!AVAILABLE_OPTIONS[@]}; do
			# If not a key=value pair, lets consider it a requested arch
			if ! [[ ${AVAILABLE_OPTIONS[i]} =~ ^.*=.* ]]; then
				REQUESTED_ARCHS="$REQUESTED_ARCHS ${AVAILABLE_OPTIONS[i]}"
				continue
			fi
		done
		[ -z "$REQUESTED_ARCHS" ] && REQUESTED_ARCHS="${ARCH}"

		# If target arch is listed in arches list
		if [[ "$REQUESTED_ARCHS" =~ (^|[[:space:]])$ARCH($|[[:space:]]) ]]; then
			get_package ${PACKAGE}
			dpkg --extract ${PACKAGE}_*_${ARCH}.deb ${EXTRACT}
		fi
	done < "${repository}/external-binaries.cfg"
}

test_repository_requires_binaries () {
	[ -f "${repository}/external-binaries.cfg" ] || return 1
	return 0
}

update_test_repository () {
	# Parse available options
	# Format is <package> <file> <arch1>,<arch2>,prefix=<relative path>,filename=<filename>
	# If an arch is specified, all non specified archs will be skipped
	# If prefix is specified, the file will copied to target project under
	# <arch>/<relative path>. It is copied below "<arch>/bin" if no prefix
	# is specified
	# If filename is specified, use it as the file name
	while read -r line ; do
		# skip lines starting with "#"
		[[ "$line" =~ ^#.*$ ]] && continue

		IFS=" " read -r PACKAGE GLOB AVAILABLE_OPTIONS <<< $line
		IFS="," read -r -a AVAILABLE_OPTIONS <<< $AVAILABLE_OPTIONS

		local PREFIX=
		local FILENAME=
		local REQUESTED_ARCHS=
		for i in ${!AVAILABLE_OPTIONS[@]}; do
			# If not a key=value pair, lets consider it a requested arch
			if ! [[ ${AVAILABLE_OPTIONS[i]} =~ ^.*=.* ]]; then
				REQUESTED_ARCHS="$REQUESTED_ARCHS ${AVAILABLE_OPTIONS[i]}"
				continue
			fi

			IFS="=" read -r -a REQUESTED_OPTIONS <<< ${AVAILABLE_OPTIONS[i]}
			local OPTION_KEY=$(trim "${REQUESTED_OPTIONS[0]}")
			local OPTION_VALUE=$(trim "${REQUESTED_OPTIONS[1]}")
			case $OPTION_KEY in
				prefix) PREFIX="$OPTION_VALUE" ;;
				filename) FILENAME="$OPTION_VALUE" ;;
				*) ;;
			esac
		done

		# skip binaries not available for this arch
		if [ ! -z "$REQUESTED_ARCHS" ]; then
			if ! [[ "$REQUESTED_ARCHS" =~ (^|[[:space:]])$ARCH($|[[:space:]]) ]]; then
				echo "skipping ${GLOB} for ${ARCH}"
				continue
			fi
		fi

		for FILE in ${EXTRACT}/${GLOB}; do
			local DIR="bin"
			[ -n "$PREFIX" ] && DIR=$PREFIX
			local TARGET="${ARCH}/${DIR}"
			mkdir -p "${repository}/${TARGET}"

			if [ -n "$FILENAME" ]; then
				TARGET="${TARGET}/${FILENAME}"
			else
				TARGET="${TARGET}/$(basename ${FILE})"
			fi

			cp ${FILE} ${repository}/${TARGET}

			git -C ${repository} add "${TARGET}"

			# Prepare the commit message (with a new line)
			echo $(ls ${PACKAGE}_*_${ARCH}.deb)" ${TARGET}" >> ${TEST}-commit-msg.txt
		done
	done < "${repository}/external-binaries.cfg"
}

# cleanup the target directory to not include the stalled binaries
cleanup_test_repository () {
    local ARCH=$1
    [ -z "${ARCH}" ] && exit 1
    [ -d "${repository}/${ARCH}" ] && rm -rf "${repository}/${ARCH}" || :
}

commit_all () {
	local GIT="git -C ${repository}"
	local CHANGES=$(${GIT} diff-index --name-only HEAD --)

	if [ -n "${CHANGES}" ]; then
		${GIT} config --local "user.email" "testbot@apertis.org"
		${GIT} config --local "user.name" "Apertis Test Bot"
		${GIT} commit -s -F "${workdir}/${TEST}-commit-msg.txt" -- ${ARCHS}
		if [ -z "$dry_run" ]; then
			${GIT} push origin HEAD:${BRANCH}
		fi
	fi
}

if ! test_repository_requires_binaries ${repository}; then
	echo "Nothing to do for ${repository}"
	exit 0
fi

echo "Binary update ${TEST} $(date +%Y%m%d)" > ${TEST}-commit-msg.txt
echo >> ${TEST}-commit-msg.txt

for ARCH in ${ARCHS} ; do
	EXTRACT="extract_${ARCH}"

	download_packages_description

	download_extract_packages

	cleanup_test_repository ${ARCH}

	update_test_repository
done

commit_all

echo "Done"
